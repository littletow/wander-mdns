package main

import (
	"context"
	"flag"
	"fmt"
	slog "log"
	"os"
	"os/signal"
	"strings"
	"time"
	"wander-mdns/apis"

	"github.com/brutella/dnssd"
	"github.com/brutella/dnssd/log"
)

var instanceFlag = flag.String("Name", "wander", "Service name")
var serviceFlag = flag.String("Type", "_http._tcp", "Service type")
var domainFlag = flag.String("Domain", "local", "domain")
var portFlag = flag.Int("Port", 9000, "Port")
var interfaceFlag = flag.String("Interface", "", "Network interface name")
var verboseFlag = flag.Bool("Verbose", false, "Verbose logging")

var timeFormat = "15:04:05.000"

func main() {
	// 命令行参数解析
	flag.Parse()
	if len(*instanceFlag) == 0 || len(*serviceFlag) == 0 || len(*domainFlag) == 0 {
		flag.Usage()
		return
	}

	if *verboseFlag {
		log.Debug.Enable()
	}

	fmt.Println("启动http服务")
	go startHTTPService()
	fmt.Println("启动mdns服务")

	instance := fmt.Sprintf("%s.%s.%s.", strings.Trim(*instanceFlag, "."), strings.Trim(*serviceFlag, "."), strings.Trim(*domainFlag, "."))

	fmt.Printf("注册服务 %s 端口 %d\n", instance, *portFlag)
	fmt.Printf("时间: –––%s–––\n", time.Now().Format("2006 1 2 "))
	fmt.Printf("%s	...注册中...\n", time.Now().Format(timeFormat))

	ctx, cancel := context.WithCancel(context.Background())
	defer cancel()

	if resp, err := dnssd.NewResponder(); err != nil {
		fmt.Println(err)
	} else {
		ifaces := []string{}
		if len(*interfaceFlag) > 0 {
			ifaces = append(ifaces, *interfaceFlag)
		}

		cfg := dnssd.Config{
			Name:   *instanceFlag,
			Type:   *serviceFlag,
			Domain: *domainFlag,
			Port:   *portFlag,
			Ifaces: ifaces,
		}
		srv, err := dnssd.NewService(cfg)
		if err != nil {
			slog.Fatal(err)
		}

		go func() {
			stop := make(chan os.Signal, 1)
			signal.Notify(stop, os.Interrupt)
			// 获取退出命令
			<-stop
			cancel()
		}()

		go func() {
			time.Sleep(1 * time.Second)
			handle, err := resp.Add(srv)
			if err != nil {
				fmt.Println(err)
			} else {
				fmt.Printf("%s	收到响应 %s: 服务已经注册并激活\n", time.Now().Format(timeFormat), handle.Service().ServiceInstanceName())
			}
		}()
		err = resp.Respond(ctx)

		if err != nil {
			fmt.Println(err)
		}
	}

}

func startHTTPService() {
	r := apis.SetupRouter()
	addr := fmt.Sprintf(":%d", *portFlag)
	r.Run(addr)
}
