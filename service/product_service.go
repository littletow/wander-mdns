package service

import (
	"bytes"
	"context"
	"fmt"
	"io"
	"log"
	"mime/multipart"

	"github.com/google/uuid"
)

// ProductService 产品服务
type ProductService struct {
	productStore ProductStore
	imageStore   ImageStore
}

// NewProductService 创建一个产品服务
func NewProductService(productStore ProductStore, imageStore ImageStore) *ProductService {
	return &ProductService{productStore, imageStore}
}

// CreateProductRequest 创建商品请求
type CreateProductRequest struct {
	Product *Product
}

// CreateProductResponse 创建商品响应
type CreateProductResponse struct {
	ID string
}

// FindProductRequest 查找商品请求
type FindProductRequest struct {
	ID      string
	BarCode string
}

// FindProductResponse 查找商品响应
type FindProductResponse struct {
	Product *Product
}

// UploadProductImageRequest 上传图片请求
type UploadProductImageRequest struct {
	ProductID string
	ImageType string
	SrcFile   *multipart.FileHeader
}

// UploadProductImageResponse 上传图片响应
type UploadProductImageResponse struct {
	ImageID string
}

// CreateProduct 增加产品
func (srv *ProductService) CreateProduct(ctx context.Context, req *CreateProductRequest) (*CreateProductResponse, error) {
	product := req.Product
	// 有产品ID，检查是否伪造
	if len(product.ID) > 0 {
		_, err := uuid.Parse(product.ID)
		if err != nil {
			return nil, fmt.Errorf("产品ID不是一个有效的ID: %v", err)
		}
	} else {
		id, err := uuid.NewRandom()
		if err != nil {
			return nil, fmt.Errorf("uuid 生成错误: %w", err)
		}
		product.ID = id.String()
	}
	err := srv.productStore.Save(product)
	if err != nil {
		return nil, err
	}

	log.Printf("成功存储产品，ID为: %s", product.ID)
	resp := &CreateProductResponse{
		ID: product.ID,
	}
	return resp, nil
}

// FindProduct 查找产品，或通过Barcode或通过ID
func (srv *ProductService) FindProduct(ctx context.Context, req *FindProductRequest) (*FindProductResponse, error) {
	id := req.ID
	barcode := req.BarCode
	// 有产品ID，检查是否伪造
	if len(id) > 0 {
		_, err := uuid.Parse(id)
		if err != nil {
			return nil, fmt.Errorf("产品ID不是一个有效的ID: %v", err)
		}
		product, err := srv.productStore.FindByID(id)
		if err != nil {
			return nil, err
		}
		resp := &FindProductResponse{
			Product: product,
		}
		return resp, nil
	}
	if len(barcode) > 0 {
		product, err := srv.productStore.FindByBarcode(barcode)
		if err != nil {
			return nil, err
		}
		resp := &FindProductResponse{
			Product: product,
		}
		return resp, nil
	}

	return nil, fmt.Errorf("查找参数为空")
}

// UploadImage 上传图片
func (srv *ProductService) UploadImage(ctx context.Context, req *UploadProductImageRequest) (*UploadProductImageResponse, error) {
	_, err := srv.productStore.FindByID(req.ProductID)
	if err != nil {
		return nil, err
	}
	imageData := bytes.Buffer{}
	src, err := req.SrcFile.Open()
	if err != nil {
		return nil, err
	}
	defer src.Close()
	_, err = io.Copy(&imageData, src)

	if err != nil {
		return nil, err
	}

	imageID, err := srv.imageStore.Save(req.ProductID, req.ImageType, imageData)

	if err != nil {
		return nil, err
	}
	resp := &UploadProductImageResponse{
		ImageID: imageID,
	}
	return resp, nil
}
