module wander-mdns

go 1.15

require (
	github.com/brutella/dnssd v1.2.0
	github.com/gin-gonic/gin v1.6.3
	github.com/google/uuid v1.2.0
	github.com/jinzhu/copier v0.2.8
	github.com/miekg/dns v1.1.27 // indirect

)
